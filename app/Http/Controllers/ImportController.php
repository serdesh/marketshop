<?php

namespace App\Http\Controllers;

use App\Contact;
use App\CsvData;
use App\Http\Requests\CsvImportRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\VarDumper\VarDumper;

class ImportController extends Controller
{
    const PATH_UPLOAD = '/system/upload/tmp/';

    public function getImport()
    {
        return view('import');
    }

    public function parseImport(CsvImportRequest $request)
    {
        $authKey = $request->authkey;

        session_start();

        $_SESSION['authkey'] = $authKey;

        $storing = false;

        $apiVer = 'v2.1.0';

        $geo_id = 213;

        $data = array();

        $path = $request->file('csv_file')->getRealPath();

        $file = fopen($path, "r");

        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Authorization: $authKey"
            ]
        ];

        while (($row = fgetcsv($file, 0, ",")) !== FALSE) {
            //Dump out the row for the sake of clarity.
            //echo 'array:';
            array_push($data, explode(";", $row[0]));
        }

        fclose($file);

        $i = 0;

        if ($request->exportToDB == 0) {
            return view('import_fields', compact('data', 'i', 'apiVer', 'geo_id', 'storing', 'opts'));
        } else {

            $out = fopen("uploads/tmp/dataXLS.xls", 'w');
            $outCSV = fopen("uploads/tmp/dataCSV.csv", 'w');
            //Пишем заголовки полей
            $titleCSV = ['Артикул поставщика', 'Закупочная цена', 'Розничная цена', 'Список/признак', 'ID товара на Яндекс Маркете', 'Наименование', 'Описание', 'Изображение', 'Ссылка на Я.Маркет'];
            fputcsv($outCSV, $titleCSV, chr(59));

            foreach ($data as $row) {

                $i = -1;
                $xlrow = [];
                $csvRow = [];

                foreach ($row as $value) {

                    $i++;


                    switch ($i) {

                        case(0):

                            $xlrow[0] = $value;
                            $csvRow[0] = $value;
                            break;

                        case(4):

                            $context = stream_context_create($opts);
                            $json = false;
                            $json = file_get_contents("https://api.content.market.yandex.ru/$apiVer/models/$value?geo_id=$geo_id", false, $context);

                            $obj = json_decode($json);

                            array_push($xlrow, $value);

                            array_push($xlrow, $obj->model->name);

                            array_push($xlrow, $obj->model->description);

                            array_push($xlrow, $obj->model->photo->url);

                            //Формируем csv для импорта в 1C Битрикс

                            array_push($csvRow, $obj->model->id);
                            array_push($csvRow, $obj->model->name);
                            array_push($csvRow, $obj->model->description);
//                          array_push($csvRow, $obj->model->photo->url);
                            $fileName = $this->downloadFile($obj->model->photo->url, microtime());
                            if ($fileName != false) {
                                array_push($csvRow, self::PATH_UPLOAD . $fileName);
                            }

                            array_push($csvRow, $obj->model->link);
                            break;

                        default:
                            array_push($xlrow, $value);
                            array_push($csvRow, $value);
                            break;
                    }


                    if ($i == 4) {
                        fputcsv($out, $xlrow, "\t");
                        fputcsv($outCSV, $csvRow, chr(59));
                    }
                }

            }

            fclose($out);
            fclose($outCSV);

            return view('import_success');
        }

    }


    public function processImport(Request $request)
    {
        $data = CsvData::find($request->csv_data_file_id);
        $csv_data = json_decode($data->csv_data, true);
        foreach ($csv_data as $row) {
            $contact = new Contact();
            foreach (config('app.db_fields') as $index => $field) {
                if ($data->csv_header) {
                    $contact->$field = $row[$request->fields[$field]];
                } else {
                    $contact->$field = $row[$request->fields[$index]];
                }
            }
            $contact->save();
        }

        return view('import_success');
    }

    /**
     * @val $url Адрес файла-картинки
     * @val $filename новое имя файла без расширения
     * @val string $folder папка для загрузки изображения
     * @return bool
     */
    private function downloadFile($url, $filename, $folder = "uploads/tmp/img/")
    {

        $ext = $this->getExtFromTypeFile($url);
        $path = $folder . $filename . '.' . $ext ;
        $ReadFile = fopen($url, "rb");
        if ($ReadFile) {
            $WriteFile = fopen($path, "wb");
            if ($WriteFile) {
                while (!feof($ReadFile)) {
                    fwrite($WriteFile, fread($ReadFile, 4096));
                }
                fclose($WriteFile);
            } else {
                return false;
            }
            fclose($ReadFile);
        } else {
            return false;
        }

        //Отправляем на сервер

        if (!$this->sendFtp($path, $filename)) {
            return false;
        }

        return $filename;
    }

    /**
     * @var $sourceFile Файл источник
     * @var $nameTargetFile Имя файла без расширения
     * @return bool
     */
    private function sendFtp($sourceFile, $nameTargetFile)
    {
        $file = $sourceFile;
        $ext = $this->getExtFromPath($file);
        $ftp_server = '77.222.57.49';
        $ftp_user_name = 'next812ru_content';
        $ftp_user_pass = 'NEXT812.RU!';
        $name = $nameTargetFile . '.' . $ext;
        $paths = 'public_html' . self::PATH_UPLOAD;
        mkdir($paths);

        $conn_id = ftp_connect($ftp_server);
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

        if ((!$conn_id) || (!$login_result)) {
            echo "Ошибка FTP соеднинения!";
            // echo "Попытка подключения к $ftp_server пользователем: $ftp_user_name";
            return false;
        }

        ftp_pasv($conn_id, true);

        $upload = ftp_put($conn_id,  $paths . '/' . $name, $file, FTP_BINARY);
        // проверяем статус загрузки
        if (!$upload) {
            echo "Error: FTP upload has failed!";
            return false;
        } else {
            echo "Good: Uploaded $name to $ftp_server";
        }
        ftp_close($conn_id);
        return true;
    }

    /**
     * Функция получает расширение файла из имени файла или пути к файлу
     * @param $fileName
     * @return mixed
     */
    function getExtFromPath($fileName)
    {
        return end(explode(".", $fileName));
    }

    /**
     * Функция получает расширение картинки исходя из Mime-type
     * @param $filename
     * @return bool|string
     */
    private function getExtFromTypeFile($filename)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $filename);
        switch ($type) {
            case ('image/gif'):
                return 'gif';

            case ('image/jpg'):
                return 'jpg';

            case ('image/pjpeg'):
                return 'jpg';

            case ('image/png'):
                return 'png';
        }
        return false;
    }

}
