<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Список полученых товаров:</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('import_process')); ?>">
                        <div class="card">

                            @foreach ($data as $row)

                                <?php $i=-1; ?>

                                @if ($row[0] !== 'test')
                                <hr>
                                @endif
                            @foreach ($row as $value)

                            <?php $i++; ?>

                            @if ($row[0] !== 'test')

                            @switch($i)

                            @case(0)

                            <p>Артикул поставщика:{{ $value }}</p>

                            @break

                            @case(1)

                            <p>Закупочная цена:{{ $value }}</p>

                            @break

                            @case(2)

                            <p>Розничная цена:{{ $value }}</p>

                            @break

                            @case(3)

                            <p>Список/признак:{{ $value }}</p>

                            @break

                            @case(4)

                            <?php

                            $context = stream_context_create($opts);

                            $json = file_get_contents("https://api.content.market.yandex.ru/$apiVer/models/$value?geo_id=$geo_id", false, $context);

                            $obj = json_decode($json); ?>



                            <p>ID товара на Y.M.: {{ $value }}</p>

                            <p>Название товара: {{ $obj->model->name }}</p>

                            <p>Описание товара: {{ $obj->model->description }}</p>

                            <p>Картинка: <img src="{{ $obj->model->photo->url }}" style="max-height: 100px;"></p>

                            <p>Город: {{ $obj->context->region->name }}</p>

                            @break

                            @endswitch


                            @endif



                            @endforeach

                            <?php $i = -1; ?>
                                    @if ($row[0] !== 'test')
                                        <hr><br><br>
                                    @endif
                            @endforeach

                        </div>

                        <a class="btn btn-warning" href="{{ url()->previous() }}">
                            Вернуться
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>