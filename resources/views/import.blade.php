@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Импорт CSV</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import_parse') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                                <label for="csv_file" class="col-md-4 control-label">CSV файл для импорта</label>

                                <div class="col-md-6">

                                    <input id="csv_file" type="file" class="form-control" name="csv_file" required>

                                    @if ($errors->has('csv_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('csv_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        Читать из CSV
                                    </button>
                                    </div>

                                    <div class="col-md-6">
                                    <p><input type="radio" name="exportToDB" value="0"> - <label> Просмотр результатов </label></p>


                                    <p><input type="radio" name="exportToDB" value="1" checked> - <label> Экспорт в Excel </label></p>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label class="col-md-12"> Aythorisation key </label>
{{--                                    <input type="text" name="authkey" class="col-md-12" placeholder="{{ $_SESSION['authkey'] }}" required>--}}
                                    <input type="text" name="authkey" class="col-md-12" placeholder="nothing" required>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
